/****************************************************************************************
 * Copyright (c) 2015 The Regents of the University of Bologna.
 * This code has been developed in the context of the
 * FP7 ICT COLOMBO project under the Framework Programme,
 * FP7-ICT-2011-8, grant agreement no. 318622.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software must display
 * the following acknowledgement: ''This product includes software developed by the
 * University of Bologna and its contributors''.
 * 4. Neither the name of the University nor the names of its contributors may be used to
 * endorse or promote products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ''AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ***************************************************************************************/

/****************************************************************************************
 * Author Federico Caselli <f.caselli@unibo.it>
 * University of Bologna 2015
 * FP7 ICT COLOMBO project under the Framework Programme,
 * FP7-ICT-2011-8, grant agreement no. 318622.
***************************************************************************************/

#include "message-id-tag.h"
#include "ns3/uinteger.h"

namespace ns3
{

	MessageIdTag::MessageIdTag()
	{
	}

	MessageIdTag::~MessageIdTag()
	{
	}

	TypeId MessageIdTag::GetTypeId(void)
	{
		static TypeId tid = TypeId("ns3::MessageIdTag").SetParent<Tag>().AddConstructor<MessageIdTag>().AddAttribute(
				"messageId", "Id of the message", UintegerValue(0), MakeUintegerAccessor(&MessageIdTag::Get),
				MakeUintegerChecker<uint32_t>());
		return tid;
	}

	TypeId MessageIdTag::GetInstanceTypeId(void) const
	{
		return GetTypeId();
	}

	uint32_t MessageIdTag::GetSerializedSize(void) const
	{
		return 4;
	}

	void MessageIdTag::Serialize(TagBuffer i) const
	{
		i.WriteU32(m_messageId);
	}

	void MessageIdTag::Deserialize(TagBuffer i)
	{
		m_messageId = i.ReadU32();
	}

	void MessageIdTag::Set(uint32_t messageId)
	{
		m_messageId = messageId;
	}

	uint32_t MessageIdTag::Get() const
	{
		return m_messageId;
	}

	void MessageIdTag::Print(std::ostream &os) const
	{
		os << "MessageId=" << m_messageId;
	}

} /* namespace ns3 */
